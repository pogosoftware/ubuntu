## Access token
You can provide access token using `-var access_token=<you_token_here>` or you can add environment variable `VAGRANT_CLOUD_TOKEN=<you_token_here>`

## Build Ubuntu 18.04 base image
```
packer build --var-file=ubuntu-18.04.json ubuntu.json
```

## Build Ubuntu 18.04 with docker
```
packer validate --var-file=ubuntu-18.04-docker.json ubuntu.json
```

## Build image without pushing it to the vagrant cloud

Add parameter `-var vagrant-no-release=true` to build Vagrant image without uploding it to the cloud.